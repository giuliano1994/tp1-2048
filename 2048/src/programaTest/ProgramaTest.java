package programaTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import negocio.Tablero;

public class ProgramaTest {

	Tablero t;
	Tablero perdio;
	Tablero noPerdio;
	Tablero jugadas;
	
	@Before
	public void crearTablero() throws Exception
	{
		
		t = new Tablero();
		perdio = new Tablero();
		noPerdio = new Tablero();
		jugadas = new Tablero();
		
		t.agregarNumeroATablero(3, 1, 4);
		t.agregarNumeroATablero(2, 2, 8);
		t.agregarNumeroATablero(3, 1, 128);
		t.agregarNumeroATablero(3, 3, 64);
		t.agregarNumeroATablero(3, 2, 64);
		
		perdio.agregarNumeroATablero(0, 0, 1);
		perdio.agregarNumeroATablero(0, 1, 2);
		perdio.agregarNumeroATablero(0, 2, 3);
		perdio.agregarNumeroATablero(0, 3, 4);
		perdio.agregarNumeroATablero(1, 0, 5);
		perdio.agregarNumeroATablero(1, 1, 6);
		perdio.agregarNumeroATablero(1, 2, 7);
		perdio.agregarNumeroATablero(1, 3, 8);
		perdio.agregarNumeroATablero(2, 0, 9);
		perdio.agregarNumeroATablero(2, 1, 10);
		perdio.agregarNumeroATablero(2, 2, 11);
		perdio.agregarNumeroATablero(2, 3, 12);
		perdio.agregarNumeroATablero(3, 0, 13);
		perdio.agregarNumeroATablero(3, 1, 14);
		perdio.agregarNumeroATablero(3, 2, 15);
		perdio.agregarNumeroATablero(3, 3, 16);		

		noPerdio.agregarNumeroATablero(0, 0, 1);
		noPerdio.agregarNumeroATablero(0, 1, 2);
		noPerdio.agregarNumeroATablero(0, 2, 3);
		noPerdio.agregarNumeroATablero(0, 3, 4);
		noPerdio.agregarNumeroATablero(1, 0, 5);
		noPerdio.agregarNumeroATablero(1, 1, 6);
		noPerdio.agregarNumeroATablero(1, 2, 7);
		noPerdio.agregarNumeroATablero(1, 3, 8);
		noPerdio.agregarNumeroATablero(2, 0, 9);
		noPerdio.agregarNumeroATablero(2, 1, 10);
		noPerdio.agregarNumeroATablero(2, 2, 11);
		noPerdio.agregarNumeroATablero(2, 2, 12);
		noPerdio.agregarNumeroATablero(3, 0, 13);
		noPerdio.agregarNumeroATablero(3, 1, 14);
		noPerdio.agregarNumeroATablero(3, 2, 16);
		noPerdio.agregarNumeroATablero(3, 3, 16);
		
	}
		
	
	
	@Test
	public void agregarVerticeTest() 
	{
		
		assertEquals(t.getPosicion(3, 1), 128);
	}
	
	
	
	@Test
	public void agregarCantidadDeVerticeTest() 
	{	
		int cont = 0;
		
		for (int i = 0; i < t.getTamanio(); i++) 
		{
			for (int j = 0; j < t.getTamanio(); j++) 
			{
				if (t.getPosicion(i, j) != 0) 
				{
					cont ++;
				
				}
			}
		}	
		
		assertTrue(cont >= 5 && cont <= 7);
	}
	
	
	
	@Test
	public void quitarNumeroDeVerticeTest() {		
			
		assertEquals(t.getPosicion(1, 1), 0);
	}
	
	
	
	@Test
	public void getPosicionTest()
	{
		assertTrue(t.getPosicion(2, 2) == 8);
	}
	
	
	
	@Test
	public void perdioTest() 
	{	
		assertTrue(perdio.getPerdio());		
	}	
	
	
	@Test
	public void noPerdioTest() 
	{
		assertFalse(noPerdio.getPerdio());		
	}
	
	
	@Test
	public void ganoTest() 
	{
		t.agregarNumeroATablero(3, 3, 2048);
		assertTrue(t.getGano());
	}		
	
	
	
	@Test
	public void esCeroTest() 
	{
		assertTrue(t.getPosicion(2, 1) == 0);		
	}		
	
	
	
	@Test
	public void noEsCeroTest() 
	{
		assertFalse(t.getPosicion(3, 1) == 0);	
	}
	
	
	
	@Test
	public void puntajeAlcanzadoTest() {
		
		t.jugar(2);
	
		assertTrue(t.getPuntajeAlcanzado() == 128);
		
	}

	@Test
	public void posicionDisponibleTest() {
		
		boolean hayPosicion = t.posicionDisponible();
		
		assertTrue(hayPosicion);
	}
	
	@Test
	public void posicionNoDisponibleTest() {
		
		boolean hayPosicion = perdio.posicionDisponible();
		
		assertFalse(hayPosicion);
	}

}
