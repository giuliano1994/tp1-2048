package programaTest;

import static org.junit.Assert.*;

import org.junit.Test;

import negocio.Tablero;

public class TesteoDeMovimiento 
{

	@Test
	public void movimientoDerechaTest()
	{
		Tablero actual = new Tablero();
		
		actual.agregarNumeroATablero(0, 0, 2);	//2 2 0 0	0 0 0 4
		actual.agregarNumeroATablero(1, 0, 2);	//0 2 2 0	0 0 0 4
		actual.agregarNumeroATablero(1, 1, 2);	//0 0 2 2	0 0 0 4
		actual.agregarNumeroATablero(2, 1, 2);	//2 0 0 2	0 0 0 4
		actual.agregarNumeroATablero(2, 2, 2);
		actual.agregarNumeroATablero(3, 2, 2);
		actual.agregarNumeroATablero(0, 3, 2);
		actual.agregarNumeroATablero(3, 3, 2);
		
		actual.jugar(3);
		
		assertEquals(actual.getPosicion(3, 0), 4);
	}
	
	
	
	@Test
	public void movimientoIzquierdaTest()
	{
		Tablero actual = new Tablero();
		
		
		actual.agregarNumeroATablero(0, 0, 2);	//2 2 0 0	4 0 0 0
		actual.agregarNumeroATablero(1, 0, 2);	//0 2 2 0	4 0 0 0
		actual.agregarNumeroATablero(1, 1, 2);	//0 0 2 2	4 0 0 0
		actual.agregarNumeroATablero(2, 1, 2);	//2 0 0 2	4 0 0 0
		actual.agregarNumeroATablero(2, 2, 2);
		actual.agregarNumeroATablero(3, 2, 2);
		actual.agregarNumeroATablero(0, 3, 2);
		actual.agregarNumeroATablero(3, 3, 2);
				
		actual.jugar(1);
		
		assertEquals(actual.getPosicion(0, 0), 4);
	}
	
	
	
	@Test
	public void movimientoArribaTest()
	{
		Tablero actual = new Tablero();
		
		actual.agregarNumeroATablero(0, 0, 2);	//2 2 0 0	4 4 4 4
		actual.agregarNumeroATablero(1, 0, 2);	//0 2 2 0	0 0 0 0
		actual.agregarNumeroATablero(1, 1, 2);	//0 0 2 2	0 0 0 0
		actual.agregarNumeroATablero(2, 1, 2);	//2 0 0 2	0 0 0 0
		actual.agregarNumeroATablero(2, 2, 2);
		actual.agregarNumeroATablero(3, 2, 2);
		actual.agregarNumeroATablero(0, 3, 2);
		actual.agregarNumeroATablero(3, 3, 2);
		
		actual.jugar(5);
		
		assertEquals(actual.getPosicion(0, 0), 4);
	}
	
	
	
	@Test
	public void movimientoAbajoTest()
	{
		Tablero actual = new Tablero();

		
		actual.agregarNumeroATablero(0, 0, 2);	//2 2 0 0	0 0 0 0
		actual.agregarNumeroATablero(1, 0, 2);	//0 2 2 0 >	0 0 0 0
		actual.agregarNumeroATablero(1, 1, 2);	//0 0 2 2 >	0 0 0 0
		actual.agregarNumeroATablero(2, 1, 2);	//2 0 0 2	4 4 4 4
		actual.agregarNumeroATablero(2, 2, 2);
		actual.agregarNumeroATablero(3, 2, 2);
		actual.agregarNumeroATablero(0, 3, 2);
		actual.agregarNumeroATablero(3, 3, 2);
		
		actual.jugar(2);
		
		assertEquals(actual.getPosicion(3, 3), 4);
		
	}
	
	
	
	@Test
	public void casosProblematicos1 ()
	{
		Tablero actual = new Tablero();
	
		actual.agregarNumeroATablero(0, 0, 2);	//2 2 4 4	0 0 4 8
		actual.agregarNumeroATablero(1, 0, 2);	//4 4 2 2 >	0 0 8 4
		actual.agregarNumeroATablero(2, 0, 4);	//2 2 2 2 >	0 0 4 4
		actual.agregarNumeroATablero(3, 0, 4);	//4 4 4 2	0 4 8 2
		actual.agregarNumeroATablero(0, 1, 4);
		actual.agregarNumeroATablero(1, 1, 4);
		actual.agregarNumeroATablero(2, 1, 2);
		actual.agregarNumeroATablero(3, 1, 2);
		actual.agregarNumeroATablero(0, 2, 2);
		actual.agregarNumeroATablero(1, 2, 2);
		actual.agregarNumeroATablero(2, 2, 2);
		actual.agregarNumeroATablero(3, 2, 2);
		actual.agregarNumeroATablero(0, 3, 4);
		actual.agregarNumeroATablero(1, 3, 4);
		actual.agregarNumeroATablero(2, 3, 4);
		actual.agregarNumeroATablero(3, 3, 2);
		
		actual.jugar(3);
		
		assertEquals(actual.getPosicion(3, 1) , 4);
		assertEquals(actual.getPosicion(2, 1) , 8);
	}
	
	
	
	@Test
	public void casosProblematicos2 ()
	{
		Tablero actual = new Tablero();
		
		actual.agregarNumeroATablero(0, 0, 2);	//2 4 8 2	2 4 8 2
		actual.agregarNumeroATablero(1, 0, 4);	//0 0 0 0 >	0 0 0 0
		actual.agregarNumeroATablero(2, 0, 8);	//4 2 0 2 >	0 0 4 4
		actual.agregarNumeroATablero(3, 0, 2);	//2 4 4 4	0 2 4 8
		actual.agregarNumeroATablero(0, 1, 0);
		actual.agregarNumeroATablero(1, 1, 0);
		actual.agregarNumeroATablero(2, 1, 0);
		actual.agregarNumeroATablero(3, 1, 0);
		actual.agregarNumeroATablero(0, 2, 4);
		actual.agregarNumeroATablero(1, 2, 2);
		actual.agregarNumeroATablero(2, 2, 0);
		actual.agregarNumeroATablero(3, 2, 2);
		actual.agregarNumeroATablero(0, 3, 2);
		actual.agregarNumeroATablero(1, 3, 4);
		actual.agregarNumeroATablero(2, 3, 4);
		actual.agregarNumeroATablero(3, 3, 4);
		
		actual.jugar(3);
		
		assertEquals(actual.getPosicion(3, 0), 2);
		assertEquals(actual.getPosicion(3, 3), 8);
	}
	
	
	
	@Test
	public void problematicosArribaTest()
	{
		Tablero actual = new Tablero();
		
		actual.agregarNumeroATablero(0, 0, 2);	//2 2 2 2	4 4 4 4
		actual.agregarNumeroATablero(1, 0, 2);	//2 2 0 2	8 4 8 2
		actual.agregarNumeroATablero(2, 0, 2);	//4 2 2 2	0 0 0 4
		actual.agregarNumeroATablero(3, 0, 2);	//4 2 8 4	0 0 0 0
		actual.agregarNumeroATablero(0, 1, 2);
		actual.agregarNumeroATablero(1, 1, 2);
		actual.agregarNumeroATablero(2, 1, 0);
		actual.agregarNumeroATablero(3, 1, 2);
		actual.agregarNumeroATablero(0, 2, 4);
		actual.agregarNumeroATablero(1, 2, 2);
		actual.agregarNumeroATablero(2, 2, 2);
		actual.agregarNumeroATablero(3, 2, 2);
		actual.agregarNumeroATablero(0, 3, 4);
		actual.agregarNumeroATablero(1, 3, 2);
		actual.agregarNumeroATablero(2, 3, 8);
		actual.agregarNumeroATablero(3, 3, 4);
		
		actual.jugar(5);
		
		assertEquals(actual.getPosicion(0, 0), 4);
		assertEquals(actual.getPosicion(0, 1), 8);
	}
	
	
	
	@Test
	public void problematicoTest2()
	{
		Tablero actual = new Tablero();
		
		actual.agregarNumeroATablero(0, 0, 8);		// 8 4 4 2  >	0 8 8 2	
		actual.agregarNumeroATablero(1, 0, 4);		// 0 0 0 0  >	0 0 0 0
		actual.agregarNumeroATablero(2, 0, 4);		// 0 0 0 0  >	0 0 0 0
		actual.agregarNumeroATablero(3, 0, 2);		// 0 0 0 0  >	0 0 0 0
		
		actual.jugar(3);
		
		assertEquals(actual.getPosicion(3, 0), 2);
		assertEquals(actual.getPosicion(2, 0), 8);
		assertEquals(actual.getPosicion(1, 0), 8);
	}
 
	
}
