package negocio;

import java.util.Arrays;
import java.util.Random;


public class Tablero 
{
 
	private int [][] tablero;
	private int contadorDeMovimientos;
	private int puntajeAlcanzado;
	private int tamanio;
	private int limite;
	private boolean gano;
	private boolean perdio;
	
	//Constructor crea una matriz de 4*4
	public Tablero () 
	{	
		tablero = new int [4][4];
		this.contadorDeMovimientos = 0;
		this.puntajeAlcanzado = 0;
		this.tamanio = tablero.length;
		this.limite = 0;
		this.gano = false;
		this.perdio = false;
		
		//si recien se inicia el juego agrega 2 aleatorios
		agregarNumeroAleatorio();
		agregarNumeroAleatorio();
	}
	
	
	//Metodos

	public void aumentarPuntuacion (int suma) 
	{
		puntajeAlcanzado = puntajeAlcanzado + suma;
		
		
	}
	
	
	private int contarMovimientos() 
	{
		if (posicionDisponible()) {
			contadorDeMovimientos++;
		}
		return contadorDeMovimientos;
	}
	
	
	private boolean perdio() 
	{
		
		
		for (int x = 0; x < tamanio; x++) 
		{
			
			for (int y = 0; y< tamanio-1; y++) 
			{
			
				if (tablero[x][y] == 0 || tablero[x][y] == tablero[x][y+1]) 
				{
					perdio = false;
					return perdio;
				}
			}
			
		}
		
		for (int y = 0; y < tamanio; y++) 
		{
			
			for (int x = 0; x< tamanio-1; x++) 
			{
			
				if (tablero[x][y] == 0 || tablero[x][y] == tablero[x+1][y]) 
				{
					
					perdio = false;
					return perdio;
				}
			}
			
		}
		
		perdio = true;
		return perdio;
	}

		
	private boolean gano() 
	{
		for (int i = 0; i < tamanio ; i++) 
		{
			for (int j = 0; j < tamanio; j++) 
			{
				if (tablero[i][j] == 2048) 
				{
					gano = true;
				}
			}
		}
	
		return gano;
	}
	
	
	//Comienzo de m�todos para mover para las cuatro direcciones.
	//Aclaracion de la variable limite. Se usa para delimitar el tablero, es decir, hasta donde puede un casillero moverse.
	//Tambien resuelve el problema de no combinar un casillero que ya fue combinado con otro. Se actualiza cada vez que recorre una fila o una columna.
	
	public void jugar(int direccion)
	{	
		if (!gano() && !perdio()) {
			//El tablero/matriz, visto como un plano de ejes cartesianos.
			//   ----------> (x)
			// |     0    1    2    3 
			// | 0 (0,0)(1,0)(2,0)(3,0) 
			// | 1 (0,1)(1,1)(2,1)(3,1)
			// | 2 (0,2)(1,2)(2,2)(3,2)
			// | 3 (0,3)(1,3)(2,3)(3,3)
			// V							
			//(y)
			contarMovimientos();
			
			switch (direccion) {
			case 1:
				this.jugadaIzquierda();	
				break;
			case 2:
				this.jugadaAbajo();	
				break;
			case 3:
				this.jugadaDerecha();
				break;
			case 5:
				this.jugadaArriba();
				break;
		
			default:
				break;
			}
			
				agregarNumeroAleatorio();
		}
	}
	
	private void jugadaAbajo()
	{
		for (int x = 0; x <= tamanio - 1; x++)		//Recorro la matriz desde abajo para arriba
		{
			limite = tamanio - 1;					//Establezco el l�mite, que va a ser hasta donde pueden desplazarse los casilleros
			for (int y = tamanio - 2; y >= 0; y--)	//Empiezo desde tamanio - 2 porque los casilleros de abajo del tablero no pueden bajar
			{
				if (tablero[x][y] != 0)				//Si un casillero es igual a 0 esta vacio, por lo tanto no puede desplazarse.
					moverAbajo(x, y);
			}
		}
	}

	private void jugadaArriba()
	{
		for (int x = 0; x <= tamanio - 1; x++)		//Recorro la matriz desde arriba hacia abajo
		{
			limite = 0;								//El limite es 0 porque es el tope del tablero.
			for (int y = 1; y < tamanio; y++)		//Empiezo desde y = 1 porque los que estan en y = 0 no pueden subir.
			{
				if (tablero[x][y] != 0)
					moverArriba(x, y);	
			}	
		}
	}
	
	private void jugadaDerecha()						//Recorro la matriz de derecha a izquierda
	{
		for (int y = 0; y <= tamanio - 1; y++)
		{
			limite = tamanio - 1;
			for (int x = tamanio -2; x >= 0; x--)
			{
				if (tablero[x][y] != 0)
					moverDerecha(x, y);						
			}
		}
	}
	
	private void jugadaIzquierda()					//Recorro la matriz de izquierda a derecha
	{
		for (int y = 0; y <= tamanio - 1; y++)
		{
			limite = 0;
			for (int x = 1; x < tamanio; x++)
			{
				if (tablero[x][y] != 0)
					moverIzquierda(x, y);
			}
		}
	}
	
	

	private void moverAbajo (int x, int y)
	{
		if (y == limite)
			return;								//tablero[x][y+1] apunta al casillero que esta justo debajo de tablero[x][y]
		if (tablero[x][y + 1] == 0)				//Si el de abajo es igual a 0, pongo el actual en cero y el de abajo pasa a valer lo que valia el actual
		{
			tablero[x][y + 1] = tablero[x][y];
			tablero[x][y] = 0;
			moverAbajo (x, y + 1);				//Utilizo recursion para que el casillero baje hasta donde le corresponda
		}
		if (tablero[x][y + 1] == tablero[x][y])	//Si el casillero de abajo es igual al actual, entonces borro el actual y multiplico el de abajo
		{
			tablero[x][y] = 0;
			tablero[x][y + 1] = tablero[x][y + 1]* 2;
			aumentarPuntuacion(tablero[x][y+1]);
			if (tablero[x][y + 1] != 0)
			{
				if (y == 1)						//Aca salvaguardo el casillero que se acaba de combinar con otro, modificando el limite asi no vuelve a combinarse.
					limite--;
				limite--;
			}
			return;
		}
	}
	
	private void moverArriba (int x, int y)
	{	
		if (y == limite)
			return;		
		if (tablero[x][y - 1] == 0)			//tablero[x][y-1] apunta al casillero que est� arriba de tablero[x][y]
		{
			tablero[x][y - 1] = tablero[x][y];
			tablero[x][y] = 0;
			moverArriba (x, y -1);
		}	
		if (tablero[x][y - 1] == tablero[x][y])
		{
			tablero[x][y] = 0;
			tablero[x][y - 1] = tablero[x][y - 1] *2;
			aumentarPuntuacion(tablero[x][y-1]);
			if (tablero[x][y - 1] != 0)
			{
				if (y == 2)
					limite++;
				limite++;
			}
			return;
		}
	}

	private void moverDerecha (int x, int y)
	{
		if (x == limite)
			return;		
		if (tablero[x + 1][y] == 0)				//tablero[x+1][y] apunta al casillero que est� a la derecha de tablero[x][y]
		{
			tablero[x + 1][y] = tablero[x][y];
			tablero[x][y] = 0;
			moverDerecha (x + 1, y);
		}	
		if (tablero[x + 1][y] == tablero[x][y])
		{
			tablero[x][y] = 0;
			tablero[x + 1][y] = tablero[x + 1][y] * 2;
			aumentarPuntuacion(tablero[x+1][y]);
			if (tablero[x + 1][y] != 0)
			{
				if (x == 1)
					limite--;			
				limite--;
			}	
			return;
		}
	}
	
	private void moverIzquierda (int x, int y)
	{
		if (x == limite)
			return;
		if (tablero[x - 1][y] == 0)				//tablero[x-1][y] apunta al casillero que est� a la izquierda de tablero[x][y]
		{
			tablero[x - 1][y] = tablero[x][y];
			tablero[x][y] = 0;
			moverIzquierda (x - 1, y);
		}
		if (tablero[x - 1][y] == tablero[x][y])
		{
			tablero[x][y] = 0;
			tablero[x - 1][y] = tablero[x - 1][y] *2;
			aumentarPuntuacion(tablero[x-1][y]);
			if (tablero[x - 1][y] != 0)
			{
				if (x == 2)
					limite++;
				limite++;
			}
			return;
		}
	}
	
	//Final de los metodos para mover para las cuatro direcciones.	
			
	private int crearNumeroAleatorio () 
	{
		Random azar = new Random();
		
		int num = azar.nextInt(2) +1;
		
		return num*2;
	}
			
	private int crearPosicionAleatoria () 
	{
		
		Random azar2 = new Random();
		
		int num = azar2.nextInt(4);    

		return num;
		
	}
			
	private void agregarNumeroAleatorio () 
	{
		int azar = crearNumeroAleatorio();		
		int fila = crearPosicionAleatoria();				
		int columna = crearPosicionAleatoria();
				
		if (posicionDisponible()) {
			while (tablero[fila][columna] != 0) 
			{				
				fila = crearPosicionAleatoria();					
				columna = crearPosicionAleatoria();		
			}		
				agregarNumeroATablero(fila, columna, azar);
		}
	}
	
	
	public void agregarNumeroATablero(int fila, int columna, int valor) {
		
		tablero[fila][columna] = valor;
		
	}
	
	
	public boolean posicionDisponible() {
		
		for (int x = 0; x < tamanio; x++) 
		{
			for (int y = 0; y< tamanio-1; y++) 
			{
				if (tablero[x][y] == 0) 
				{
					return true;
				}
			}
		}
		
		return false;
	}
	
	
	// Fin de los m�todos usados para jugar.
	
	
	
	// geters and seters
		
	public int getTamanio() {
		return this.tamanio;
	}

	public int getPosicion (int x, int y)
	{
		return this.tablero[x][y];
	}
		
	public int getContadorDeMovimientos() {
		return contadorDeMovimientos;
	}
	
	public boolean getGano() {
		return gano();
	}

	public void setGano(boolean gano) {
		this.gano = gano;
	}

	public boolean getPerdio() {
		return perdio();
	}

	public void setPerdio(boolean perdio) {
		this.perdio = perdio;
	}

	public int getPuntajeAlcanzado() {
		
		return puntajeAlcanzado;
	}

	public void setPuntajeAlcanzado(int puntajeAlcanzado) {
		this.puntajeAlcanzado = puntajeAlcanzado;
	}


	public String toString()
	{
		String ret = "";
		for (int y = 0; y < tamanio; y++)
		{
			ret = ret + " \n ";
			for (int x = 0; x < tamanio; x++)
				ret = ret + this.tablero[x][y] + " ";
		}
		return ret;
	} 
		
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(tablero);
		result = prime * result + tamanio;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tablero other = (Tablero) obj;
		if (!Arrays.deepEquals(tablero, other.tablero))
			return false;
		if (tamanio != other.tamanio)
			return false;
		return true;
	}
	
}
