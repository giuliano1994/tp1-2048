package ventana;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;


import negocio.Tablero;

import javax.swing.JTable;

public class VentanaUsuario {

	private JFrame frame;
	private JTextField txtNombre;
	private JTable table;
	private Tablero tablero;

	private KeyListener keyListener;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaUsuario window = new VentanaUsuario();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	/**
	 * Create the application.
	 */
	public VentanaUsuario() {
		initialize();
	}

	// Metodos para utilizar la gui
	public void populateTable() {
		// DefaultTableModel model = (DefaultTableModel) table.getModel();
		DefaultTableModel tableModel = new DefaultTableModel();

		int tam = tablero.getTamanio();

		// TODO: Quitar hardcode
		tableModel.addColumn("0");
		tableModel.addColumn("1");
		tableModel.addColumn("2");
		tableModel.addColumn("3");
		
		for (int y = 0; y < tam; y++) {
			Object rowData[] = new Object[tam];
			for (int x = 0; x < tam; x++) {
				rowData[x] = tablero.getPosicion(x, y);
			}
			tableModel.addRow(rowData);
		}

		table.setModel(tableModel);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		txtNombre = new JTextField();
		txtNombre.setToolTipText("ingresar nombre");
		txtNombre.setBounds(283, 119, 130, 19);

		frame.getContentPane().add(txtNombre);
		txtNombre.setColumns(10);

		JLabel lblPuntaje = new JLabel("Puntaje:");
		lblPuntaje.setBounds(12, 12, 70, 15);
		frame.getContentPane().add(lblPuntaje);

		final JLabel puntaje = new JLabel("");
		puntaje.setBounds(78, 12, 70, 15);
		frame.getContentPane().add(puntaje);
		
		JLabel lblMovimientos = new JLabel("Movimientos:");
		lblMovimientos.setBounds(202, 12, 94, 15);
		frame.getContentPane().add(lblMovimientos);

		final JLabel movimientos = new JLabel("");
		movimientos.setBounds(308, 12, 70, 15);
		frame.getContentPane().add(movimientos);

		table = new JTable();
		table.setBounds(58, 54, 199, 175);
		frame.getContentPane().add(table);

		final JLabel lblNombre = new JLabel("Nombre : ");
		lblNombre.setBounds(283, 94, 130, 14);
		frame.getContentPane().add(lblNombre);

		// Variables
		tablero = new Tablero();
		// Controlador tablero
		keyListener = new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO: Solo puede acceder al switch si btnComenzar es visible				
				
				if (!tablero.getGano()) {
					
					if (!tablero.getPerdio()) {
						
						switch (e.getKeyCode()) {
						case KeyEvent.VK_UP:
							// PRESIONO ARRIBA
							tablero.jugar(5);
							populateTable();
							break;
		
						case KeyEvent.VK_DOWN:
							// PRESIONO ABAJO
							tablero.jugar(2);
							populateTable();
							break;
		
						case KeyEvent.VK_RIGHT:
							// PRESIONO DERECHA
							tablero.jugar(3);
							populateTable();
							break;
		
						case KeyEvent.VK_LEFT:
							// PRESIONO IZQUIERDA
							tablero.jugar(1);
							populateTable();
							break;
		
						default:
							// NINGUNA DE LAS ANTERIORES
							break;
							}
						}
						else {
							
							new Perdio(tablero.getPuntajeAlcanzado()).setVisible(true);
						}
				}
				else {
					
					new Gano(tablero.getPuntajeAlcanzado()).setVisible(true);
				}
			
				String puntos = String.valueOf(tablero.getPuntajeAlcanzado());
				puntaje.setText(puntos);
					
				String mov = String.valueOf(tablero.getContadorDeMovimientos());
				movimientos.setText(mov);
			}
		};
		
		final JButton btnFinalizar = new JButton("Finalizar");
		btnFinalizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				System.exit(0);
				
			}
		});
		btnFinalizar.setBounds(296, 164, 105, 25);
		frame.getContentPane().add(btnFinalizar);
		//oculto boton finalizar
		btnFinalizar.setVisible(false);

		final JButton btnComenzar = new JButton("Comenzar");
		// Al presionar boton comenzar
		btnComenzar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Validaciones
				if (txtNombre.getText().length()>0) {
					btnComenzar.setVisible(false);
					btnFinalizar.setVisible(true);

					populateTable();

					table.addKeyListener(keyListener);
					
					lblNombre.setText(lblNombre.getText() + " " + txtNombre.getText());
					
					txtNombre.setVisible(false);
					
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Debe ingresar un nombre");
				}
			}
		});

		btnComenzar.setBounds(296, 200, 105, 25);
		frame.getContentPane().add(btnComenzar);

	}
}
