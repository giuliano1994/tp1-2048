package ventana;

import java.awt.BorderLayout;
//import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

//import negocio.Tablero;

//import negocio.Tablero;

import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Gano extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Gano dialog = new Gano();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 * @param puntajeAlcanzado 
	 */
	public Gano(int puntajeAlcanzado) {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JButton Reiniciar = new JButton("Reiniciar");
			Reiniciar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					VentanaUsuario.main(null);
					
				}
			});
			Reiniciar.setBounds(12, 239, 192, 25);
			contentPanel.add(Reiniciar);
		}
		{
			JLabel lblNewLabel = new JLabel("!!!! Felicidades ah Ganado !!!!");
			lblNewLabel.setBounds(118, 12, 231, 84);
			contentPanel.add(lblNewLabel);
		}
		{
			JButton Finalizar = new JButton("Finalizar");
			Finalizar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
				
					System.exit(0);
				
				}
			});
			Finalizar.setBounds(246, 239, 192, 25);
			contentPanel.add(Finalizar);
		}
		{
			JLabel lblNewLabel_1 = new JLabel("Tu puntuacion es:");
			lblNewLabel_1.setBounds(34, 108, 145, 36);
			contentPanel.add(lblNewLabel_1);
		}
		{
			JLabel Puntuacion = new JLabel("");
			Puntuacion.setBounds(232, 119, 70, 15);
			contentPanel.add(Puntuacion);
			
			String p = String.valueOf(puntajeAlcanzado);
			
			Puntuacion.setText(p);
		}
	}

	public Gano() {
		// TODO Auto-generated constructor stub
	}

}
