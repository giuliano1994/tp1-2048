package ventana;

//import java.awt.BorderLayout;
//import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
//import javax.swing.JPanel;
//import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;



import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Perdio extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JLabel Puntuacion = new JLabel("");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Perdio dialog = new Perdio();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 * @param puntajeAlcanzado 
	 */
	public Perdio(int puntajeAlcanzado) {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		{
			JLabel lblNewLabel = new JLabel("Que pena haz perdido, vuelve a intentarlo");
			lblNewLabel.setBounds(67, 34, 323, 43);
			getContentPane().add(lblNewLabel);
		}
		{
			JLabel lblNewLabel_1 = new JLabel("Tu puntuacion es:");
			lblNewLabel_1.setBounds(67, 109, 145, 36);
			getContentPane().add(lblNewLabel_1);
		}
		{
			JButton Reintentar = new JButton("Reintentar");
			Reintentar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					VentanaUsuario.main(null);
				
				}
			});
			Reintentar.setBounds(36, 227, 145, 25);
			getContentPane().add(Reintentar);
		}
		{
			JButton Finalizar = new JButton("Finalizar");
			Finalizar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					System.exit(0);
				}
			});
			Finalizar.setBounds(245, 227, 145, 25);
			getContentPane().add(Finalizar);
		}
		Puntuacion.setBounds(235, 113, 112, 32);
		getContentPane().add(Puntuacion);
		
		String p = String.valueOf(puntajeAlcanzado);
		
		Puntuacion.setText(p);
	}

	public Perdio() {
		// TODO Auto-generated constructor stub
	}

}
